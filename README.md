This project, called FILM (Friendly Interface to Learning Machines), aims to set up a machine learning pipeline that is easy to use for researchers without much coding and/or machine learning experience.


### How do I get set up? ###

This is an R package, so first get some R. Then inside R, do the following command:

install.packages([PATH],repos = NULL)

where [PATH] is the path to where you downloaded this package.

Currently, this pipeline depends on the following R-packages:

* tools
* oro.nifti
* caret

The file IO module also supports various FreeSurfer formats, if this software is installed.
Note that FreeSurfer needs to be reachable, which usually requires sourcing FreeSurferSetUp.(c)sh.
The best approach is to start R (or any front-end, like Rstudio) from a terminal that has sourced FreeSurfer.

### To do ###

Stuff on the project to do list

* Remove FreeSurfer dependency (translate their MatLab IO functions to R) [LOW PRIORITY]

### Who do I talk to? ###

For more information, contact janssen dot rj at gmail dot com